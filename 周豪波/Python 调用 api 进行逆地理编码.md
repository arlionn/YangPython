# Python 调用 API 进行逆地理编码  

> 作者：周豪波（西北大学）
> 邮箱：963564360@qq.com

上一篇推文中，我们已经介绍过了地理编码，当输入地址而返回坐标时，也就是当我们输入一个建筑物名字或一个地址的时候，地图返回一个点坐标，这个过程叫地理编码；那么反过来，当输入的是点坐标而返回的是一个地址描述，这个过程叫做 **逆地理编码**。  

实际运用中，地理编码和逆地理编码可以在产业集聚得到很广泛的应用，产业集聚是区域经济发展的一个重要模式。它在全球化的背景下发展起来，是一种地理集聚并在某一特定领域内相互关联的，在地理位置上集中的公司和机构的集合。作为一种空间组织形式的产业集聚在过去的几十年间得到了迅速的发展并成为经济发展的热点问题。故而，产业集聚分析也常常在区域或城市产业宏观分析中占据比较重要的一席。  

通过地理编码，我们可以得到初步名称与经纬度坐标，但在实证研究中，我们就希望得到这些企业各自更加详细精确的描述，例如所处的区域，详细分类其周边道路信息等，以便进行进一步的分析和探索，这在地图底层服务里面实际是用到了逆地理编码。  
  
下面，就让我们介绍调用 API 的另一种应用—— **逆地理编码**。  

## 1. 逆地理编码 

> 逆地理编码服务提供将坐标点（经纬度）转换为对应位置信息（如所在行政区划，周边地标点分布）功能。

调用百度地图开放平台逆地理编码的 API  即可实现由经纬度坐标转为详细地址。调用时的 URL 如下：
>- http://api.map.baidu.com/reverse_geocoding/v3/?ak=********&output=json&coordtype=wgs84ll&location=31.225696563611,121.49884033194 
>- *代表个人在百度地图开放平台申请的 ak 码；coordtype=wgs84ll 设置的是输入坐标系的类型，可参考百度开放平台中的 [逆地理编码服务文档](https://lbsyun.baidu.com/index.php?title=webapi/guide/webservice-geocoding-abroad) 进行设置。

## 2. Python 程序
下面我们附上代码来实现 API 的调用操作：

### S1：调用库，构建抓取的根 URL 并确定工作目录
```python
from urllib.request import urlopen   #调用库，向网页发送请求
import json                          #调用库，json是一种便于解析的格式
import time                          #调用库，time是时间，防止访问次数过于频繁被系统后台禁止

baseURL = 'http://api.map.baidu.com/reverse_geocoding/v3/?'   #地理编码的url
ak = '*********'                                              #在百度地图中申请的ak

path = "C:\\Users\\hp\\Desktop\\111\\"  #地址文件所在的路径，Windows系统的分隔符为\\，mac系统的分隔符是/
codeList = []                           #创建一个code列表存储转换出来的坐标
```
### S2：定义抓取动作并读取坐标文件，生成抓取队列。 
特别要注意的是在进行循环抓取之前，我们要首先进行单次抓取，查看返回结果。若为字典形式，可以直接提取其结果信息，若为列表形式。需要先将其中的字典形式提取出来，再进行提取。
```python
#抓取动作分四步：
#访问-读取-解析-休眠
def fetch(url):                  #定义抓取的参数是url
    feedback = urlopen(url)      #使用 urllib2 库里的 urlopen 命令进行访问url
    data = feedback.read()       #读取 url
    response = json.loads(data)  #对返回结果进行解析
    time.sleep(2)                #暂停2秒，防止访问次数过于频繁被系统后台禁止
    return response              #返回抓取结果

coordeFile = open(path + 'code8.txt', 'r')      #打开地址文件，r是读取的意思
outputFile = path + 'reverse_code.txt'          #定义输出的文件名
coorde = coordeFile.readlines()                 #读取坐标文件中的每一行
```

### S3：循环coorde，对每一个coorde进行fetch，并存储所得数据。  
需要值得注意的是，由于我们需要进行多次逆地理编码，所以我们在这里用到了for循环，对坐标进行循环抓取。

```python
for c in range(0, 2):          #循环coorde中的0-9行地址
    # 提取地址
    coo = coorde[c].split(',') #提取每行的信息
    coo_lng = coo[1].strip()   #0对应坐标文件中的第1列，1对应坐标文件中的第2列
    coo_lat = coo[2].strip()
    locator = coo[0]           #定位我们运行到了第几个坐标
    print('This is ' + str(locator) + ' of ' + str(len(coorde)))  #在屏幕打印运行到了第几个坐标
    
    URL = baseURL + 'ak=' + ak + '&output=json' + '&coordtype=wgs84ll' + '&location=' + str(coo_lat) + ',' + str(coo_lng)
    response = fetch(URL)                  #返回抓取的 url的数据
    contents = response['result']          #将结果赋给contents
    
    # 开始提取字典中的所需信息
    lng = contents['location']['lng']              #提取经度        
    lat = contents['location']['lat']              #提取纬度 
    add = contents['formatted_address']            #提取详细地址
    codeInfo = str(lng)+','+str(lat)+','+str(add)  #定义输出的结果为经度+纬度+地址
    codeList.append(codeInfo)                      #抓取到一个codeInfo便加到codeList列表中
print(codeList)
```
### S4：生成一个txt文件，输出结果
```python
with open(outputFile, 'a') as f:
    for codeInfo in codeList:
        f.write (str(codeInfo) + '\n')
f.close()
```
通过上述代码，我们就可以顺利实现逆地理编码了。  

 ## 3. 正/逆地理编码的应用示例
 本篇介绍了通过 Python 调用 API 进行逆地理编码的操作，地理编码与逆地理编码两者相辅相成，通过地理编码，我们可以将详细地址量化为坐标数据，并进行可视化表现分析；通过逆地理编码，我们可以将位置坐标解析成对应的行政区划数据以及周边高权重地标地点分布情况，整体描述坐标所在的位置。   
 
我们还是运用上一次介绍过的**ArcGIS**，以西安市酒店行业为例，对两者应用进行示例：  

#### 应用地理编码将地址信息可视化：
![Fig01](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TIM图片20200225180517.png "Fig01-西安市酒店分布图")

#### 西安市酒店行业集聚分析
![Fig02](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TIM图片20200225180512.png  "Fig02")

#### 应用逆地理编码对点数据信息进行查询：
![Fig03](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TIM图片20200225180527.png "Fig03")