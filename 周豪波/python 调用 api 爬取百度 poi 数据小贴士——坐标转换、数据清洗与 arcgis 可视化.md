# Python 调用 API 爬取百度 POI 数据小贴士之坐标转换、数据清洗与可视化

> 作者：周豪波（西北大学）
> 邮箱：963564360@qq.com

上一篇推文中，我们主要介绍了 Python 调用 API 爬取百度 POI 数据的主要操作过程和代码解析。但在实际操作中，还有几个操作事项需要注意，解决完后方可顺利完成 POI 的爬取。这篇推文就以下三个问题和大家一起讨论。

>- 百度坐标系与非百度坐标系的转换
>- 删除爬取的重复数据与处理中文乱码
>- 爬取数据在 ArcGIS 中的可视化

## 1. 非百度坐标系的转换

在调用百度 API 进行 POI 爬取时，由于默认输入坐标类型为百度坐标系（BD09ll），而我们第一手数据往往并不是百度坐标。所以，我们需要将常用的非百度坐标（目前支持 GPS 设备获取的坐标、google  地图坐标、soso 地图坐标、amap 地图坐标、mapbar 地图坐标）转换成百度地图中使用的坐标，才可将转化后的坐标在百度地图 JavaScriptAPI、静态图 API、Web 服务 API 等产品中使用。    

### 1.1 调用坐标转换 API

调用百度地图坐标转换的 API 即可实现由非百度坐标转为百度坐标。调用时的 url 如下：
>- http://api.map.baidu.com/geoconv/v1/?coords=114.21892734521,29.575429778924&from=1&to=5&ak=您的ak码 
>- 常见的非百度坐标包括 WGS84 或 GCJ02，设置转换前的坐标类型请参考百度开放平台中 [坐标转换的服务文档](https://lbsyun.baidu.com/index.php?title=webapi/guide/changeposition)

#### 运用 Python 批量转换坐标

之前我们已经尝试过调用百度 API 抓取 POI ，在这里，我们借鉴之前的思路，进行坐标转换。  
  
#### S1：调用库，构建抓取的根 URL 并确定工作目录。

```python
import urllib2            #调用爬虫所需的库
from urllib import quote  #调用 urllib 库里面的查询命令
import json               #调用库，json 是一种便于解析的格式

baseURL = 'http://api.map.baidu.com/geoconv/v1/?' #转换坐标的url
ak = '**********'                                 #在百度地图开放平台中申请的ak码

path = 'C:\\Users\\hp\\Desktop\\BaiduAPI\\' #爬取坐标文件所在的路径，Windows 系统的分隔符为\\，mac系统的分隔符是/
coordList = []                              #创建一个 coordinate 列表存储转换出来的坐标
```

#### S2：定义抓取动作并读取坐标文件，生成抓取队列：

```python
#抓取动作分四步：访问——读取——解析——休眠
def fetch(url):                     #定义抓取的参数是 url
    feedback = urllib2.urlopen(url)  #使用 urllib2 库里的 urlopen 命令进行访问 url
    data = feedback.read()           #读取 url
    response = json.loads(data)      #对返回结果进行解析
    return response                  #返回抓取结果

coordinateFile = open(path + 'XianCoord.txt', 'r') #打开坐标文件即经纬度坐标文件，r是读取的意思
outputFile = path + 'BaiduCoord05.txt'             #定义输出的 POI 文件, BaiduCoord15自行命名
coordinates = coordinateFile.readlines()           #读取坐标文件中的每一行
```

#### S3：循环 coordinates，对每一个 coordinates 进行 fetch，并进行翻页，最后存储所得数据。  

特别要注意的是在进行循环抓取之前，我们要先进行单次抓取，查看返回结果。若为字典形式，可以直接提取其结果信息，若为列表形式。需要先将其中的字典形式提取出来，再进行提取。

```python
# for coord in coordinates:
for c in range(0, 360):                #循环coordinates中的坐标，360指运行到第360行，可根据需要更改
    # 提取坐标
    coord = coordinates[c].split(',')   #提取坐标
    lng = coord[1].strip()              #纬度，1对应坐标文件中的第2列
    lat = coord[2].strip()              #经度，2对应坐标文件中的第3列
    locator = coord[0]                  #定位我们抓取到了第几个坐标，0对应坐标文件中的第1列
    print 'This is ' + str(locator) + ' of ' + str(len(coordinates))  #输出抓取到了第几个坐标
   
    URL = baseURL + 'coords=' + str(lng) + ',' + str(lat) + '&from=1&to=5' \
               + '&ak=' + ak   
        
    response = fetch(URL)               #返回抓取的 url 的数据
    contents = response['result']       #返回抓取结果
        
    # 开始循环抓取列表中的所需信息
    for content in contents:
        lng = content['x']                  #提取经度        
        lat = content['y']                  #提取纬度 
        coordInfo = str(lng)+';'+str(lat)   #定义输出的结果为经度+纬度
        print str(lng) + ',' + str(lat)     #输出经度+纬度
        coordList.append(coordInfo)         #抓取到一个 coordinateInfo 便加到 coordinateList 列表中
```
 
#### S4：生成 txt 文件，输出结果

```python
with open(outputFile, 'a') as f:
    for coordInfo in coordList:
        f.write(coordInfo.encode('utf-8') + '\n')
f.close()     
```

### 1.2 坐标系换回  

爬取完毕后，由于默认输出坐标类型为百度坐标（BD09ll），但是百度坐标系与最常用的 WGS84 坐标系或 GCJ02 坐标系之间会存在偏移，所以我们在导入 ArcGIS 进行分析之前，需要再把 POI 点的坐标转换为常用的 WGS84 坐标系或 GCJ02 坐标系，否则会对我们的研究分析造成的一定的误差。可以参考网络上的坐标转换工具包进行坐标转换，还可以通过下面方式进行坐标转换。

### 1.3 控制参数转换坐标  
百度开放平台提供了通过参数控制输入输出坐标类型。我们需要在爬取 POI 代码中调用的 url 中加入控制坐标类型的参数。  

**爬取 POI 代码** 时的 url 如下：

- **输入坐标** 为非百度坐标系时用 coord_type 参数控制 (以圆形区域进行地点检索为例)：    
http://api.map.baidu.com/place/v2/search?query=银行&location=39.915,116.404&radius=2000&output=json&ak=您的ak码&coord_type=1 或 http://api.map.baidu.com/place/v2/search?query=银行&location=39.915,116.404&radius=2000&output=json&ak=您的ak码&coord_type=2

>- 常见的非百度坐标包括 WGS84 或 GCJ02，输入坐标系为 WGS84 时 coord_type=1；输入坐标系为 GCJ02 时 coord_type=2。设置时的坐标类型请参考百度开放平台中 [地点检索的服务文档](https://lbsyun.baidu.com/index.php?title=webapi/guide/webservice-placeapi)

- **输出坐标** 为 GCJ02 坐标系时用 ret_coordtype 参数控制：  
http://api.map.baidu.com/place/v2/search?query=银行&location=39.915,116.404&radius=2000&output=json&ak=您的ak码&coord_type=2&ret_coordtype=gcj02ll


## 2. 删除爬取的重复数据

抓取工作完成后，我们就得到了一系列带有坐标和 UID 码的餐饮类 POI，由于我们在爬取 POI 时为了避免遗漏，半径的设置可能会偏大，就会出现重复的POI点。所以，接下来我们要对数据进行清理与筛选，在这里主要介绍如何删除爬取的重复数据以及对乱码的中文进行转码。我们用 `Stata` 软件实现，具体操作如下：

```stata
*打开 txt 文件（打开之前将 ; 替换为 , ）stata识别以 , 或 tab 键分隔的变量
 shellout "D:\CrawlPOI\XianChAnPOI_Restaurant_20200201.txt"                        
*导入txt文件
 insheet using "D:\CrawlPOI\XianChAnPOI_Restaurant_20200201.txt", clear           
*删除爬取的重复数据
 duplicates drop v4, force                                                    
*保存数据为stata的数据格式.dta
 save "D:\CrawlPOI\111.dta", replace                                                
*-转码之前清空内存中数据
 clear 
*-进入需要转码文件的文件夹目录
cd "D:\CrawlPOI\"
*-设置需要转码的类型为中文 (gb18030)
 unicode encoding set gb18030
*-转码为gb18030格式的编码
 unicode translate "快捷.dta"
*-打开转码后的数据文件
 use "快捷.dta"
*-保存转码后的数据文件
 save "快捷.dta", replace
*-导出数据为excel格式
*-sheet("****")：导出至****名字的sheet
*-firstrow(vari)：导出excel文件时第一行保存为变量名称
 export excel "D:\CrawlPOI\111.xlsx", sheet("restaurant") firstrow(vari) replace
```    

最终整理效果如下图：  
![Fig01](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TIM图片20200221160810.png "Fig01")

## 3. ArcGIS 可视化  
数据整理完以后，我们就完成了前期的数据搜集工作。那么我们如何对这些数据进行可视化的表达与分析呢？以刚刚得到的餐饮类 POI 为例，我们可以通过 ArcGIS 绘制其热度图并进行某片区的餐饮业密度分布检测与规律探寻。

### 3.1 数据表导入 GIS

打开 GIS，点击 `添加数据`，导入整理好的 EXCEL 数据（注: 若出现导入失败，将 EXCEL 保存成 03 版本）  


### 3.2 可视化 POI 数据

右击添加进的 EXCEL 数据，点击 `显示 XY 数据`，将 X 字段设为经度、Y 字段设为纬度。得到 POI 的初步可视化的结果并右击 `导出数据`，导出为 shp 格式的数据，并添加该文件至 GIS 图层中，如下图所示：  

![Fig02](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TIM图片20200211173043.png "Fig02")
  
这样，就可以在 ArcGIS 中进行进一步的挖掘和分析了。  
### 3.3 密度分析
密度分析可以对某个现象的已知量进行处理，然后将这些量分散到整个地表上，依据是在每个位置测量到的值和这些测量值所在位置的空间关系。  

>- 作用：密度表面可以显示出点要素或线要素较为集中的地方。例如，每个城镇都可能有一个点值，表示该镇的人口总数，但我们想更多地了解人口随地区的分布情况。由于每个城镇内并非所有人都住在聚居点上，通过计算密度，我们可以创建出一个显示整个地表上人口的预测分布状况的表面。  

#### 3.3.1 点密度分析    
>- 点密度：计算每个输出栅格像元周围的点要素的密度。可用于查明房屋、野生动物观测值或犯罪事件的密度。

`ArcToolbox`-->`Spatial Analyst 工具`-->`密度分析`-->`点密度分析`  
结果如下图所示：  

![Fig03](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TIM图片20200211173054.png "Fig03")

#### 3.3.2 核密度分析  
>- 核密度：计算要素在其周围邻域中的密度。  
可用于计算人口密度、建筑密度、获取犯罪情况报告、旅游区人口密度监测、连锁店经营情况分析等等。  

`ArcToolbox`-->`Spatial Analyst 工具`-->`密度分析`-->`核密度分析`    
结果如下图所示：  

![Fig04](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TIM图片20200211173049.png "Fig04")  

#### 辨析：   
对于点密度，需要指定一个邻域以便计算出各输出像元周围像元的密度 (可理解为在像元邻域内取点或线均值)，而核密度则可将各点的已知总体数量从点位置开始向四周分散。  

- 点密度分析：点密度分析是以研究区域内的栅格为分析单元，用于计算每个输出栅格像元周围的点要素的密度。从概念上讲，每个栅格像元中心的周围都定义了一个邻域，将邻域内点的数量相加，然后除以邻域面积，即得到点要素的密度。

- 核密度分析：核密度分析使用核函数根据点要素计算每单位面积的量值以将各个点拟合为光滑锥状表面。在核密度分析中，在各点周围生成表面所依据的二次公式可为表面中心(点位置)赋予最高值，并在搜索半径距离范围内减少到零。对于各输出像元，将计算各分散表面的累计交汇点总数。(可理解为随着与点或线的距离越远，权重越小)

## 4. 小结：
本文通过对贯穿爬取全过程的三个问题进行了讨论并作了具体介绍，其中ArcGIS可视化部分中由于篇幅限制，就不再继续展开讨论，其他的可视化与分析方法，大家可以登陆网站 [ArcGIS学习] (https://malagis.com/) 进行学习。