# Python 调用 API 进行地理编码

> 作者：孙斯嘉（长安大学）
> 邮箱：810963623@qq.com

> 平时在做数据调查的时候，我们往往只能拿到地址信息，并不方便直接进行可视化，那么如何将地址信息转化成我们所熟悉的经纬度坐标在地理信息系统中来进行可视化分析呢？

> 国内的高德、百度等地图服务商们都有提供现成的 API 接口(地理编码服务)，方便我们直接调用。本文利用地理编码服务，根据地址名称，实现批量抓取地理坐标数据。

> 用户可通过地理编码可快速查找到各类位置。可搜索的位置类型包括：感兴趣点或地名词典中的地名，例如山脉、桥梁和店铺，基于经纬度或其他参考系统的坐标，以及可通过各种样式和格式表示的地址，其中包括街道交叉口、含有街道名称的门牌号及邮政编码。地址结构越完整，地址内容越准确，解析的坐标精度越高，详情可点击 [百度开放平台地理编码服务介绍](http://lbsyun.baidu.com/index.php?title=webapi/guide/webservice-geocoding) 查询。

## 解析地址程序代码

首先我们要导入程序所需要的库，构建所要抓取的根 url ，根据百度开放平台中的服务文档的说明确定 url 的内容，可点击 [百度开放平台地理编码服务文档](http://lbsyun.baidu.com/index.php?title=webapi/guide/webservice-geocoding) 查询，接下来我们要确定存放解析地址出来的坐标文件的工作目录。
```python

from urllib.request import urlopen #调用urllib.request库里面的urlopen命令，向网页发送请求并打开网页
import json #调用库，json是一种便于解析的格式
import time #调用库，time是时间，防止访问次数过于频繁被系统后台禁止

baseURL = 'http://api.map.baidu.com/geocoding/v3/?' #地理编码的url
ak = '********************' #在百度地图中申请的ak

path = "E://pachong//GeoCoding//" #地址文件所在的路径，Windows系统的分隔符为\\，mac系统的分隔符是/
codeList = [] #创建一个code列表存储解析出来的坐标
```

之后我们定义一个`fetch`小程序用于抓取 url 链接，接下来读取地址文件，生成抓取列表。循环所生成的 address 列表，对列表中的每一个 address 进行 fetch
下一步进行实际的抓取动作，根据 BaseURL ，构建抓取所用 URL，生成抓取结果并将结果赋给 contents 。

我们在编写程序之前，需要手动单次解析地址文件，获取其坐标，访问相应 url 后，我们发现 contents 的格式是字典，因此我们在编写程序时，可以对字典中的信息进行直接提取。由于 contents 中包含较多信息，我们只对所需信息进行提取，即地址的经纬度、是否精确查找、提取地址的误差范围、提取地址理解程度以及地址类型。
```python

# 对url数据进行抓取，分四步：访问-读取-解析-休眠
def fetch(url): #定义抓取的参数是url
    feedback = urlopen(url) #使用urllib2库里的urlopen命令进行访问url
    data = feedback.read()  #读取url
    response = json.loads(data) #对返回结果进行解析
    time.sleep(2) #暂停2秒，防止访问次数过于频繁被系统后台禁止
    return response #返回抓取结果
    
addressFile = open(path + 'address.txt', 'r') #打开地址文件，r是读取的意思
outputFile = path + '01txt' #定义输出的文件名
address = addressFile.readlines() #读取地址文件中的每一行

import urllib.parse #调用库，实现url的识别和分段
for c in range(0, 10): #循环address中的0-9行地址
    add = address[c].split(',') #提取每行的信息
    addvalue = add[1].strip() #0对应地址文件中的第1列，1对应地址文件中的第2列
    locator = add[0] #定位我们运行到了第几个地址
    print('This is ' + str(locator) + ' of ' + str(len(address))) #在屏幕打印运行到了第几个地址
    URL = baseURL + 'address=' + urllib.parse.quote(addvalue) + '&output=json' + '&ak=' + ak   
    response = fetch(URL) #返回抓取的url的数据
    contents = response['result'] #将结果赋给contents
    
    lng = contents['location']['lng'] #提取经度        
    lat = contents['location']['lat'] #提取纬度 
    prec = contents['precise'] #提取位置的附加信息，是否精确查找。1为精确查找，即准确打点；0为不精确，即模糊打点
    conf = contents['confidence'] #提取地址的误差范围，分值范围0-100，分值越大，解析误差绝对精度越高
    compr = contents['comprehension'] #提取地址理解程度，分值范围0-100，分值越大，服务对地址理解程度越高
    lev = contents['level'] #能精确理解的地址类型，包含：UNKNOWN、国家、省、城市、区县、收费处/收费站 、加油站、绿地、门址等
    codeInfo = str(lng)+','+str(lat)+','+str(prec)+',' \
               +str(conf)+','+str(compr)+','+str(lev) #定义输出的结果为经度+纬度+0/1+提取坐标点的误差范围+提取地址理解程度+地址类型
    print(str(lng) + ',' + str(lat)) 
    codeList.append(codeInfo) #抓取到一个codeInfo便加到codeList列表中
print(codeList)
```
最后，生成一个 txt 文件，输出结果
```python

with open(outputFile, 'a') as f:
    for codeInfo in codeList:
        f.write (str(codeInfo) + '\n')
f.close()
```
## 小结
本程序主要通过构建一个`fetch`小程序抓取网页中的 url 链接，并对输入的地址文件中的每一个地址进行循环、提取目标参数，最终生成地理坐标文件，完成整个地理转码的过程。需要注意的是返回的坐标系类型是 BD09 坐标系，需要转换为常用的 GCJ02 或 WGS84 坐标系。若我们想转换为 GCJ02 坐标系，可以在 url 中添加控制参数 `ret_coordtype = gcj02ll`；若我们想转换为 WGS84 坐标系，可以参考网络上的坐标转换工具包。

地理编码是地理建模的基础。而地理建模，是为了将现实世界中的对象进行抽象，以计算机的方式表达的过程。只有对地理对象进行建模，才能通过计算机模拟和分析各种地理过程，服务于人类的科学认知并解决实际问题。因此，我们在人文社会学科的调查中均可以使用地理编码服务来获得地理坐标数据。